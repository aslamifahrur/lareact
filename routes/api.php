<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function() {
    Route::get('/get_engineers','API\v1\EngineerController@get_engineers');
    Route::get('/get_data','API\v1\EngineerController@get_data');
    Route::get('/get_engineers','API\v1\KadivController@get_engineers');
    Route::get('/get_projects','API\v1\KadivController@get_projects');
    Route::get('/get_projects','API\v1\EngineerController@get_projects');
    Route::get('/get_kelompok_tugas/{project}','API\v1\KadivController@get_kelompok_tugas');
    Route::get('/get_tugas/{project}','API\v1\KadivController@get_daftar_tugas');
    Route::get('/image/{filename}', 'API\v1\EngineerController@displayImage')->name('image.displayImage');
    Route::get('/get_daftar_tugas', 'API\v1\EngineerController@get_daftar_tugas');
    Route::post('/get_dat','API\v1\EngineerController@get_dat');
    
    Route::post('/laporan_harian','API\v1\EngineerController@post_laporan_harian');
    // Untuk direksi
    Route::get('/get_tracking','API\v1\DireksiController@get_tracking');
});
