<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->to(route('home'));
});

Auth::routes();

Route::get('/logout','Auth\LoginController@logout');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('loginadmin','Auth\LoginController@postloginadmin')->name('loginadmin');
Route::get('loginadmin', function() {
    return redirect()->to(route('login'));
});

Route::get('/direksi', 'DireksiController@index')->where('any', '.*')->name('direksi');
Route::get('/direksi/{any}', 'DireksiController@index')->where('any', '.*');
Route::get('/kadiv', 'KadivController@index')->where('any', '.*')->name('kadiv');
Route::get('/kadiv/{any}', 'KadivController@index')->where('any', '.*');
Route::get('/engineer', 'EngineerController@index')->where('any', '.*')->name('engineer');
Route::get('/engineer/{any}', 'EngineerController@index')->where('any', '.*');
Route::get('/get_user',function(){
    return Auth::user()->id;
});
Route::get('/{any}',function(){
    return redirect()->to('home');
})->where('any','.*');
