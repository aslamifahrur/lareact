/*!

=========================================================
* Material Dashboard React - v1.8.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
// import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
// import LocationOn from "@material-ui/icons/LocationOn";
// import Notifications from "@material-ui/icons/Notifications";
// import Unarchive from "@material-ui/icons/Unarchive";
// import Language from "@material-ui/icons/Language";
import DashboardPage from "./views/Dashboard/Dashboard.js";
import DaftarProject from "./views/DaftarProject/DaftarProject.js";
import DaftarKaryawan from "./views/DaftarKaryawan/DaftarKaryawan.js";
import Tracker from "./views/Tracker/Tracker.js";
import Icons from "./views/Icons/Icons.js";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/direksi"
  },
  {
    path: "/daftar_project",
    name: "Daftar Project",
    icon: "content_paste",
    component: DaftarProject,
    layout: "/direksi"
  },
  {
    path: "/daftar_karyawan",
    name: "Daftar Karyawan",
    icon: Person,
    component: DaftarKaryawan,
    layout: "/direksi"
  },
  {
    path: "/tracker",
    name: "Tracker",
    icon: "desktop_windows",
    component: Tracker,
    layout: "/direksi"
  },
  {
    path: "/icons",
    name: "Icons",
    icon: BubbleChart,
    component: Icons,
    layout: "/direksi"
  }
];

export default dashboardRoutes;
