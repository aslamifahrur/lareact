import React, { useState, useEffect } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import Table from "./Table.js";
import Card from "../../components/Card/Card.js";
import CardHeader from "../../components/Card/CardHeader.js";
import CardBody from "../../components/Card/CardBody.js";
import DateFnsUtils from '@date-io/date-fns';;
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  },
  track_wrapper:{
    display:'flex',
    alignItems:'flex-start',
    justifyContent:'flex-end'
  },
  track_aplikasi:{
    display: 'inline-block',
    textAlign: 'right',
    width:'auto'
  },
  track_separator:{
    display: 'inline-block',
    textAlign:'center',
    width:'10px',
    flexShrink: '0',
  },
  track_waktu:{
    display: 'inline-block',
    width: '150px',
    flexShrink: '0',
  }
};

const useStyles = makeStyles(styles);

function getTanggal(tanggal=new Date(),separator = '-') {

  let date = tanggal.getDate();
  let month = tanggal.getMonth() + 1;
  let year = tanggal.getFullYear();

  return `${date}${separator}${month < 10 ? `0${month}` : `${month}`}${separator}${year}`
}

function DaftarEngineer() {
  const [daftar_track, setDaftarTrack] = useState([]);
  const [selectedDate, setSelectedDate] = useState(new Date());

  const handleDateChange = date => {
    setSelectedDate(date);
  };

  const classes = useStyles();

  useEffect(()=>{
    var tanggal=getTanggal(selectedDate,'-');
    fetch("http://localhost:8000/api/v1/get_tracking?tanggal="+tanggal)
    .then(res => res.json())
    .then(data => {
      var anu=data.map((item)=>{
        var hasil_track=item.hasil_track.split(";");
        var konversi=hasil_track.map((track)=>{
          var waktu,jam,menit,detik;
          var seplit=track.split(':');
          if(seplit[1]>3600){
            jam=Math.floor(seplit[1]/3600);
            menit=Math.floor((seplit[1]%3600)/60);
            detik=seplit[1]%60;
            waktu=jam+" jam "+menit+" menit "+detik+" detik";
          } else if(seplit[1]>60){
            menit=Math.floor(seplit[1]/60);
            detik=seplit[1]%60;
            waktu=menit+" menit "+detik+" detik";
          } else{
            waktu=detik+" detik";
          }
          return (
            <div className={classes.track_wrapper}>
              <div className={classes.track_aplikasi}>{seplit[0]}</div>
              <div className={classes.track_separator}>:</div>
              <div className={classes.track_waktu}>{waktu}</div>
            </div>
          )
        })
        return [item.id_pegawai,item.nama_pegawai,konversi];
      })
      setDaftarTrack(anu);
    }).catch(error => console.log(error))
  },[selectedDate])

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>Tracker Monitoring System</h4>
          </CardHeader>
          <CardBody>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                margin="normal"
                id="date-picker-dialog"
                label="Pilih Tanggal :"
                format="dd-MM-yyyy"
                value={selectedDate}
                onChange={handleDateChange}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </MuiPickersUtilsProvider>
            <Table
              tableHeaderColor="primary"
              tableHead={["ID Pegawai", "Nama Pegawai","Hasil Tracking"]}
              tableData={daftar_track}
            />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  );
}

export default DaftarEngineer;
