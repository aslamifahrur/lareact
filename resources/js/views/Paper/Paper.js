import React, { useState, useEffect } from 'react';
import { withStyles,makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Progress } from 'antd';


const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

function createData(name, calories, fat) {
  return { name, calories, fat};
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0),
  createData('Ice cream sandwich', 237, 9.0),
  createData('Eclair', 262, 16.0),
  createData('Cupcake', 305, 3.7),
  createData('Gingerbread', 356, 16.0),
];

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));





export default function CenteredGrid() {
  const classes = useStyles();
  const [count, setCount] = useState(0);
  const [karyawans, setKaryawans] = useState([]);
  const [id,setId]=useState("");
  const [nama,setNama]=useState("");
  const [alamat,setAlamat]=useState("");

  const handleAddKaryawan = () => { //onclick button effect
    fetch("http://127.0.0.1:8000/api/v1/get_data", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id: id,
        nama: nama,
        alamat: alamat
      })
    })
      .then(result => result.text())
      .then(anu => console.log(anu))
      .catch(error => console.log(error))
  }

  useEffect(() => {   //didalam use effect tidak di render ulang
    fetch("http://127.0.0.1:8000/api/v1/get_engineers", {
      method: "GET",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
      .then(result => result.json()) //dapat data dari result.jeson
      .then(anu => setKaryawans(anu)) //variabel setkaryawan diisi anu
      .catch(error => console.log(error))
  }, [])

  useEffect(() => {
    console.log(karyawans);
  }, [karyawans])



  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>xs=12</Paper>
          <TextField id="standard-basic" onChange={(event)=>setId(event.target.value)} label="id" />
          <TextField id="standard-basic" onChange={(event)=>setNama(event.target.value)} label="nama" />
          <TextField id="standard-basic" onChange={(event)=>setAlamat(event.target.value)} label="alamat" />
          <Button variant="contained" color="secondary" onClick={handleAddKaryawan}>
            Secondary
      </Button>
      <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell align="center" > No</StyledTableCell>
            <StyledTableCell align="center" > id</StyledTableCell>
            <StyledTableCell align="center">nama</StyledTableCell>
            <StyledTableCell align="center">alamat</StyledTableCell>
            {/* <StyledTableCell align="right">Carbs&nbsp;(g)</StyledTableCell>
            <StyledTableCell align="right">Protein&nbsp;(g)</StyledTableCell> */}
          </TableRow>
        </TableHead>
        <TableBody>
          {karyawans.map((row,index) => (
            <StyledTableRow key={index}>
              <StyledTableCell component="th" scope="row">
                {index+1}
              </StyledTableCell>
              <StyledTableCell component="th" scope="row">
                {row.id}
              </StyledTableCell>
              <StyledTableCell align="right"><Progress percent={30} />{row.nama}</StyledTableCell>
              <StyledTableCell align="right">{row.alamat}</StyledTableCell>
              {/* <StyledTableCell align="right">{row.carbs}</StyledTableCell>
              <StyledTableCell align="right">{row.protein}</StyledTableCell> */}
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>

        </Grid>
      </Grid>

    </div>
  );
}