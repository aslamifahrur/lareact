import React from "react";
// material-ui components
import { makeStyles } from "@material-ui/core/styles";
import CustomTabs from "../../../components/CustomTabs/CustomTabs.js";
import TableProject from "./TableProject";
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import TimerOffIcon from '@material-ui/icons/TimerOff';
import Button from "@material-ui/core/Button";
import FormAddProject from "./FormAddProject";
// import DialogDetails from "./DialogDetails";

const styles = {
    textCenter: {
        textAlign: "center"
    }
};

const useStyles = makeStyles(styles);

export default function HomePageKadep(){
    const classes = useStyles();
    const [dialogAddProjectState, setDialogAddProjectState] = React.useState(false);
    const handleDialogState = () => {
        setDialogAddProjectState(!dialogAddProjectState)
    }
    return (
        <div>
            <CustomTabs
                headerColor="primary"
                rightButton = {(
                    <div>
                    <Button onClick={handleDialogState} variant="contained" size="small" color="secondary" style={{position: "absolute", right:0, top:0, bottom:0}}>
                        Add Project
                    </Button>
                     <Button>  
                    </Button>
                    </div>
                )}
                
                tabs={[
                    {
                        tabName: "Berlangsung",
                        tabIcon: QueryBuilderIcon,
                        tabContent: (
                            <TableProject mode={1}/>
                        )
                    },
                    {
                        tabName: "Selesai",
                        tabIcon: TimerOffIcon,
                        tabContent: (
                            <TableProject mode={2}/>
                        )
                    }
                ]}
            />
            <FormAddProject open={dialogAddProjectState} handleClose={handleDialogState}/>
        </div>
    );
};
