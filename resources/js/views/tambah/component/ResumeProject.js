import React from "react";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import FormAddResumeProject from "./FormAddResumeProject";
import {host} from "../../utils/server"
// core components

export default class ResumeProject extends React.Component{
    constructor(props){
        super(props)
        this.state={
            openDialog:false,
            data:[],
            years : [2018, 2019, 2020, 2021],
            year : new Date().getFullYear()
        }
    }
    componentDidMount() {
        this.updateDataResumeProject()
    }

    handleOpen = () => {
        this.setState({
            openDialog:true
        })
    };
    handleClose = () => {
        this.setState({
            openDialog:false
        })
    };
    handleChange = (event) => {
        this.setState({
            year:event.target.value
        }, ()=>{this.updateDataResumeProject()})
    }
    updateDataResumeProject = () => {
        var query = host+"/project/read_resume?year="+this.state.year
        var setting = {
            method: 'GET'
        }
        fetch(query, setting)
            .then((respone)=>respone.json())
            .then((responeJson)=>{
                console.log(responeJson.result)
                this.setState({
                    data: responeJson.result
                })
            })
            .catch((error)=>console.log(error))
    }
    render() {
        return (
            <div>
                <Card>
                    <CardContent style={{
                        display:"flex",
                        flexDirection: "row",
                        justifyContent: "space-between"
                    }}>
                        <Typography style={{flex:2}} variant="h6" gutterBottom>Laporan Pekerjaan</Typography>
                        <TextField
                            select
                            // label="Please select year"
                            value={this.state.year}
                            onChange={this.handleChange}
                            style={{flex:1, maxWidth:100}}
                        >
                            {this.state.years.map((option, index) => (
                                <MenuItem key={index} value={option}>
                                    {option}
                                </MenuItem>
                            ))}
                        </TextField>
                    </CardContent>
                    <CardContent>
                        <GridContainer>
                            {this.state.data.map((_data, index)=>(
                                <GridItem xs={12} sm={6} md={3}>
                                    <Card style={{borderRadius:0}}>
                                        <CardActionArea>
                                            <CardMedia
                                                style={{
                                                    height:140
                                                }}
                                                image={require("../../assets/img/bg.jpg")}
                                                title="Contemplative Reptile"
                                            >
                                                <CardContent style={{bottom:0}}>
                                                    <Typography gutterBottom variant="h5" component="h2">
                                                        {_data.month}
                                                    </Typography>
                                                </CardContent>
                                            </CardMedia>
                                        </CardActionArea>
                                        <CardActions>
                                            <Button size="small" color="primary">
                                                Open
                                            </Button>
                                            <Button size="small" color="primary">
                                                Delete
                                            </Button>
                                        </CardActions>
                                    </Card>
                                </GridItem>
                            ))}
                        </GridContainer>
                    </CardContent>
                </Card>

                <Fab
                    color="primary"
                    style={{
                        position:"absolute",
                        bottom:0,
                        right:0,
                        marginBottom:40,
                        marginRight:40
                    }}
                    onClick={this.handleOpen}
                >
                    <AddIcon />
                </Fab>
                <FormAddResumeProject open={this.state.openDialog} handleClose={this.handleClose} handleOpen={this.handleOpen} update={this.updateDataResumeProject}/>
            </div>
        );
    }
}
