import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import "./FormAddProject.css"
import TableSubProject from "./TableSubProject.js";

const Jimp = require('jimp')
const moment = require('moment')

export default class DialogDetails extends React.Component{
    constructor(props){
        super(props)
        this.state={
            engineer: []
        }
    }

    render() {
        return (
            <div>
                <Dialog fullWidth={true} maxWidth={'md'} open={this.props.open} onClose={()=>this.props.handleClose()} aria-labelledby="form-dialog-title" >
                    <DialogTitle>Details Project</DialogTitle>
                    <DialogContent>
                        <TableSubProject/>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={()=>this.props.handleClose()} color="primary">
                            Kembali
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}
