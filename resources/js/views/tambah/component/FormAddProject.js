import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import "./FormAddProject.css"
import MultiSelect from "./MultiSelect.js";
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import Fab from "@material-ui/core/Fab";
import List from "@material-ui/core/List";
import AddIcon from '@material-ui/icons/Add';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from "@material-ui/icons/ExpandLess";

const Jimp = require('jimp')
const moment = require('moment')

export default class FormAddProject extends React.Component{
    constructor(props){
        super(props)
        this.state={
            project: "",
            date: moment(),
            engineer: [],
            mode:0,
            index: -1,
            dataEngineer :[
                {
                    id: 1,
                    name: "Bekti Ponco Saputro",
                    sub : []
                },
                {
                    id: 2,
                    name: "Fahrur",
                    sub : []
                },
                {
                    id: 3,
                    name: "Udin",
                    sub : []
                }
            ]
        }

        this.handleDueDate = this.handleDueDate.bind(this);
        this.handleEnginer = this.handleEnginer.bind(this);
        this.handleChangeProject = this.handleChangeProject.bind(this);
        this.handleNext = this.handleNext.bind(this);
    }
    componentDidMount() {
        this.setState({
            mode:0
        })
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps!=this.props){
            this.setState({
                mode:0
            })
        }
    }

    handleDueDate = (date) => {
        this.setState({
            date:date
        })
    }

    handleChangeProject = (event) => {
        this.setState({
            project:event.target.value
        })
    }

    handleEnginer = (event) => {
        this.setState({
            engineer:event.target.value
        }, ()=>{
            var dataEngineer = []
            event.target.value.map((engineer, index)=>(
                dataEngineer.push({
                    name: engineer,
                    sub: []
                })
            ))
            this.setState({
                dataEngineer:dataEngineer
            })
        })
    }

    handleNext = () => {
        this.setState({
            mode:1
        })
    }

    handleAddSub = (index) => {
        var dataEngin = this.state.dataEngineer;
        dataEngin[index].sub.push({sub:""})
        this.setState({
            dataEngineer:dataEngin
        })
    }

    handelSubChange = (event, index, indexSub) => {
        var dataEngin = this.state.dataEngineer;
        dataEngin[index].sub[indexSub].sub = event.target.value
        this.setState({
            dataEngineer:dataEngin
        })
    }

    handleIndex = (index) => {
        if(this.state.index==index){
            this.setState({
                index: -1
            })
        }else{
            this.setState({
                index: index
            })
        }
    }

    formSatu = () => {
        return (
            <div style={{display:"flex", flexDirection:"column", minHeight:300}}>
                <TextField
                    label="Project"
                    value={this.state.project}
                    onChange={this.handleChangeProject}
                    style={{marginBottom:20}}
                />
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                        margin="normal"
                        id="date-picker-dialog"
                        label="Due Date"
                        format="dd MMMM yyyy"
                        value={this.state.date}
                        onChange={this.handleDueDate}
                        KeyboardButtonProps={{
                            'aria-label': 'change date',
                        }}
                    />
                </MuiPickersUtilsProvider>
                <MultiSelect label={"Engineer"} onChange={this.handleEnginer} listData={this.state.engineer}/>
            </div>
        )
    }
    formDua = () => {
        return (
            <List
                component="nav"
                aria-labelledby="nested-list-subheader"
                style={{minHeight:300}}
            >
                {
                    this.state.dataEngineer.map((engineer, index)=>(
                        <div>
                            <ListItem button onClick={()=>this.handleIndex(index)}>
                                <ListItemText primary={engineer.name}/>
                                {this.state.index===index ? <ExpandLess /> : <ExpandMore />}
                            </ListItem>
                            <Collapse in={this.state.index===index} timeout="auto" unmountOnExit>
                                <div style={{display:"flex",width:"100%", flexDirection:"column"}}>
                                    {
                                        // engineer.sub.length>0?
                                        engineer.sub.map((subProject, indexSub)=>(
                                            <TextField onChange={(event)=>this.handelSubChange(event, index, indexSub)} key={indexSub} style={{marginTop:10}} id="outlined-basic" value={subProject.sub} variant="outlined" />
                                        ))
                                    }
                                    <Fab size="small" color="secondary" aria-label="add" style={{marginTop:10, alignSelf:"center"}} onClick={()=>this.handleAddSub(index)}>
                                        <AddIcon />
                                    </Fab>
                                </div>
                            </Collapse>
                        </div>
                    ))
                }
            </List>
        )
    }

    render() {
        return (
            <div>
                <Dialog fullWidth={true}
                        maxWidth={'sm'} open={this.props.open} onClose={()=>this.props.handleClose()} aria-labelledby="form-dialog-title" >
                    <DialogTitle>Tambah Project</DialogTitle>
                    <DialogContent>
                        {/*{this.formSatu()}*/}
                        {this.state.mode==0?this.formSatu():this.formDua()}
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={()=>this.props.handleClose()} color="primary">
                            Cancel
                        </Button>
                        <Button
                            onClick={this.handleNext}
                            color="primary"
                        >
                            Next
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}
