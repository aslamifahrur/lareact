import React, { useState, useEffect } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Upload, message, Button } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import Card from '@material-ui/core/Card';
import DatePickers from "./Component/Date.js";
import { Progress } from 'antd';
import 'antd/dist/antd.css';
import './Component/index.css';
import TextField from '@material-ui/core/TextField';
import { teal } from '@material-ui/core/colors';
import CustomizedDialogs from "./Component/Dialog.js";
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { MinusOutlined, PlusOutlined } from '@ant-design/icons';
import { TimePicker } from 'antd';
import Grid from '@material-ui/core/Grid';
import Slider from '@material-ui/core/Slider';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
} from '@material-ui/pickers';




const { RangePicker } = TimePicker;
const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: teal[500],
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },

}))(TableCell);


const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

function createData(name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein };
}

const rows = [
    createData('proyek A', 159, 6.0, 24, 4.0),
    createData('Proyek B', 237, 9.0, 37, 4.3),
    createData('Proyek C', 262, 16.0, 24, 6.0),
    createData('Proyek D', 305, 3.7, 67, 4.3),
    createData('Proyek E', 356, 16.0, 49, 3.9),
];

const useStyles = makeStyles({
    table: {
        minWidth: 1000,
    },
    width: '75ch',


});
function valueLabelFormat(value) {
    // const [coefficient, exponent] = value
    //   .toExponential()
    //   .split('e')
    //   .map((item) => Number(item));
    // return `${Math.round(coefficient)}e^${exponent}`;
    return `${value} %`;
}




export default function CustomizedTables() {

    const [karyawans, setKaryawans] = useState([]);
 
    const [progress, setProgress] = useState([]);
    const [refresh, setRefresh] = useState(false);
    const [daftarTugas, setDaftarTugas] = useState([]);
    const classes = useStyles();
    const [value, setValue] = React.useState(1);

    const handleChange = (event, newValue) => {
        { progress[index] = newValue; setRefresh(!refresh); }
    };
    const props = {
        name: 'file',
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };

    function getTugas() {
        fetch("http://127.0.0.1:8000/get_user")
            .then(res => res.text())
            .then(id => {
                fetch("http://127.0.0.1:8000/api/v1/get_daftar_tugas?id=" + id)
                    .then(result => result.json())
                    .then(data => {
                        console.log(data);
                        setDaftarTugas(data);
                    }).catch(error => console.log(error))
            }).catch(error => console.log(error))
    }
    function submit(index, tugasid) {
        console.log(progress[index])
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ progress: progress[index], id: tugasid })
        };
        fetch('http://127.0.0.1:8000/api/v1/post_laporan_harian', requestOptions)
            .then(response => console.log(response))

    }
    // useEffect(() => {
    //     const requestOptions = {
    //         method: 'POST',
    //         headers: { 'Content-Type': 'application/json' },
    //         body: JSON.stringify({ title: 'React Hooks POST Request Example' })
    //     };
    //     fetch('http://127.0.0.1:8000/api/v1/post_laporan_harian', requestOptions)
    //         .then(response => response.json())
    //         console.log(response);

    // },[])
    useEffect(() => {   //didalam use effect tidak di render ulang
        getTugas();
        // getDat();

        // fetch("http://127.0.0.1:8000/api/v1/get_projects", {
        //     method: "GET",
        //     headers: {
        //         'Accept': 'application/json',
        //         'Content-Type': 'application/json',
        //     }
        // })
        //     .then(result => result.json()) //dapat data dari result.jeson
        //     .then(anu => setKaryawans(anu)) //variabel setkaryawan diisi anu
        //     .catch(error => console.log(error))

    }, [])

    useEffect(() => {
        var one_day = 1000 * 60 * 60 * 24
        var kary = (karyawans);
        var a = kary.map((row, nama) => (row.deadline));
        var b = kary.map((row, nama) => (row.id_karyawan));
        //console.log(a);
         var hariini = new Date();
        // var v = new Date(a)
         var day = hariini.getDate();
        console.log("Cek hari", day);
        // var Result =a;
        // var Final_Result = Result.toFixed(0);
        console.log(b);
    }, [karyawans])

    //  const [button, setButton]=useState(()=>{
    //      console.log("run functon")
    //      return 4 
    //  });
    //  function plus (){
    //      setButton(prevButton => prevButton + 1);

    //  };
    //  function minus (){
    //      setButton(prevButton => prevButton - 1);
    //  }

    const [selectedDate, setSelectedDate] = React.useState(new Date('2014-08-18T21:11:54'));

    const handleDateChange = (date) => {
        setSelectedDate(date);
    };



    return (




        <TableContainer component={Paper}>
            <Typography>day</Typography>
            <Table className={classes.table} aria-label="customized table">

                <TableHead>
                    <TableRow>
                        <StyledTableCell align="center"> id</StyledTableCell>
                        <StyledTableCell align="center"> Kode Proyek</StyledTableCell>
                        <StyledTableCell align="center"> Kode Tugas</StyledTableCell>
                        <StyledTableCell align="center">Start Time</StyledTableCell>
                        <StyledTableCell align="center">End Time</StyledTableCell>
                        <StyledTableCell align="center">End Date</StyledTableCell>
                        <StyledTableCell align="center">days left</StyledTableCell>
                        <StyledTableCell align="center">status</StyledTableCell>

                        {/* <StyledTableCell align="center">unggah dokumen</StyledTableCell> */}
                        <StyledTableCell align="center">Submit</StyledTableCell>
                    </TableRow>
                </TableHead>

                <TableBody>
                    {daftarTugas.map((row, index) => {
                        progress.push(row.progress);
                        return (
                            <StyledTableRow key={index} align="center">
                                <StyledTableCell align="center"><Typography align="center"  >{row.id} </Typography>  </StyledTableCell>
                                <StyledTableCell align="center"><Typography align="center"  >{row.kode_project} </Typography>  </StyledTableCell>
                                <StyledTableCell align="center"><Typography align="center"  >{row.kode_tugas} </Typography>  </StyledTableCell>
                                <StyledTableCell component="th" scope="row" align="center">
                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <KeyboardTimePicker
                                            margin="normal"
                                            id="time-picker"
                                            label="Time picker"
                                            value={selectedDate}
                                            ampm={false}
                                            onChange={handleDateChange}

                                            KeyboardButtonProps={{
                                                'aria-label': 'change time',
                                            }}
                                            style={{ width: 100 }}
                                        />
                                    </MuiPickersUtilsProvider>

                                    {/* <RangePicker /> */}
                                    {/* <Typography align="center" style={{ backgroundColor: '#cfe8fc', height: '5vh' }}>{row.tanggal_mulai}</Typography> */}
                                </StyledTableCell>
                                <StyledTableCell component="th" scope="row" align="center">
                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <KeyboardTimePicker
                                            margin="normal"
                                            id="time-picker"
                                            ampm={false}
                                            label="Time picker"
                                            value={selectedDate}
                                            onChange={handleDateChange}
                                            KeyboardButtonProps={{
                                                'aria-label': 'change time',
                                            }}
                                            style={{ width: 100 }}
                                        />
                                    </MuiPickersUtilsProvider>

                                    {/* <RangePicker /> */}
                                    {/* <Typography align="center" style={{ backgroundColor: '#cfe8fc', height: '5vh' }}>{row.tanggal_mulai}</Typography> */}
                                </StyledTableCell>
                                <StyledTableCell component="th" scope="row" align="center">
                                    <Typography  >{row.deadline} </Typography>
                                </StyledTableCell>

                                <StyledTableCell align="center"><Typography align="center"  >{row.sisa_hari} hari</Typography></StyledTableCell>

                                <StyledTableCell >
                                    <Grid container spacing={1}>

                                        {/* <Progress type="circle" percent={progress[index]} width={60} />
                                <Button.Group>
                                    <Button  icon={<MinusOutlined />} onClick={()=>{progress[index]=progress[index]-1;setRefresh(!refresh);}} /> */}
                                        {/* <Button  icon={<MinusOutlined />} onClick={()=>{setDaftarTugas(prevDaftarTugas=>{var dataInput=row; dataInput=dataInput.progress-1; prevDaftarTugas[index]=dataInput; return prevDaftarTugas});setRefresh(!refresh);}} /> */}
                                        {/* <Button  icon={<PlusOutlined />} onClick={()=>{progress[index]=progress[index]+1;setRefresh(!refresh);}} />
                                </Button.Group> */}

                                        <Typography>{progress[index]}% </Typography>



                                        <Slider
                                            value={progress[index]}
                                            //value={value}
                                            min={0}
                                            step={1}
                                            max={100}
                                            //scale={(x) => x ** 10}
                                            getAriaValueText={valueLabelFormat}
                                            valueLabelFormat={valueLabelFormat}
                                            onChange={(event, newValue) => {
                                                progress[index] = newValue;
                                                setRefresh(!refresh);
                                            }}
                                            valueLabelDisplay="auto"
                                            aria-labelledby="non-linear-slider"
                                            style={{ width: 150 }}
                                        />

                                    </Grid>

                                </StyledTableCell>


                                {/* <StyledTableCell align="center">
                                    <CustomizedDialogs />
                                </StyledTableCell> */}
                                {/* <StyledTableCell align="center"><Upload {...props}><Button><UploadOutlined /> Click to Upload </Button></Upload>   </StyledTableCell> */}
                                <StyledTableCell align="center"><Button onClick={() => submit(index, row.id)}> Submit </Button>  </StyledTableCell>
                            </StyledTableRow>

                        );
                    })}

                </TableBody>
            </Table>
        </TableContainer>






    );
}
