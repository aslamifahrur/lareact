import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import 'antd/dist/antd.css';
import CardHeader from "../../components/Card/CardHeader.js";
import { Progress } from 'antd';
import Card from "../../components/Card/Card.js";
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Popover from '@material-ui/core/Popover';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import { Tooltip, Button, Divider } from 'antd';
import './index.css';



const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,

    },
    popover: {
        pointerEvents: 'none',
    },
    fab: {
        margin: theme.spacing(0),
    },
    absolute: {
        position: 'absolute',
        bottom: theme.spacing(1),
        right: theme.spacing(1),
    },
}));

export default function CenteredGrid() {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);

    const open = Boolean(anchorEl);

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={6}>
                    <Card>
                        <CardHeader color="success" >
                            <Grid container spacing={1}>
                                <Grid item xs={6}>
                                    <a>Proyek A</a>
                                </Grid>
                                <Grid item xs={2}>
                                </Grid>
                                <Grid item xs={2}>
                                    <Progress type="circle" width={60} percent={75} format={percent => `${percent} Days`}  strokeWidth="10" strokeColor="black"/>
                                </Grid>
                                <Grid item xs={2}>
                                    <Tooltip title="Add" aria-label="add" >
                                        <Fab color="primary" className={classes.fab}>
                                            <AddIcon />
                                        </Fab>
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </CardHeader>
                        {/* <Paper className={classes.paper}> */}
                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Tooltip title="aslam" color="cyan" placement="leftTop">
                                        <Avatar src="http://127.0.0.1/gambar/2.jpg" aria-owns={open ? 'mouse' : undefined}
                                        />
                                    </Tooltip>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Tooltip title='progress pekerjaan 1' >
                                        <Progress percent={30} />
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                <Tooltip title='lena'placement="leftTop">
                                    <Avatar alt="marc" src="http://127.0.0.1/gambar/111.jpg" aria-owns={open ? 'mouse' : undefined}/>
                                    </Tooltip>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Tooltip title='progress pekerjaan 2' >
                                        <Progress percent={50} status="active" aria-owns={open ? 'mouse-over-popover' : undefined} />
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Avatar>W</Avatar>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Progress percent={70} status="exception" />
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Avatar>W</Avatar>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Progress percent={100} />
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Avatar>W</Avatar>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Progress percent={50} showInfo={false} />
                                </Grid>
                            </Grid>
                        </Paper>
                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Tooltip title='aslam'>
                                        <Avatar src="http://127.0.0.1/gambar/2.jpg" aria-owns={open ? 'mouse' : undefined}
                                        />
                                    </Tooltip>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Tooltip title='progress pekerjaan 1' >
                                        <Progress percent={30} />
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </Paper>
                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Tooltip title='aslam'>
                                        <Avatar src="http://127.0.0.1/gambar/2.jpg" aria-owns={open ? 'mouse' : undefined}
                                        />
                                    </Tooltip>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Tooltip title='progress pekerjaan 1' >
                                        <Progress percent={30} />
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </Paper>

                        {/* </Paper> */}
                    </Card>
                </Grid>
                <Grid item xs={6}>
                    <Card>
                        <CardHeader color="success" >
                            <Grid container spacing={1}>
                                <Grid item xs={6}>
                                    <a>Proyek A</a>
                                </Grid>
                                <Grid item xs={2}>
                                </Grid>
                                <Grid item xs={2}>
                                    <Progress type="circle" width={60} percent={75} format={percent => `${percent} Days`} />
                                </Grid>
                                <Grid item xs={2}>
                                    <Tooltip title="Add" aria-label="add" >
                                        <Fab color="primary" className={classes.fab}>
                                            <AddIcon />
                                        </Fab>
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </CardHeader>
                        {/* <Paper className={classes.paper}> */}
                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Tooltip title='aslam'>
                                        <Avatar src="http://127.0.0.1/gambar/2.jpg" aria-owns={open ? 'mouse' : undefined}
                                        />
                                    </Tooltip>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Tooltip title='progress pekerjaan 1' >
                                        <Progress percent={30} />
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                <Tooltip title='lena'>
                                    <Avatar alt="marc" src="http://127.0.0.1/gambar/111.jpg" aria-owns={open ? 'mouse' : undefined}/>
                                    </Tooltip>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Tooltip title='progress pekerjaan 2' >
                                        <Progress percent={50} status="active" aria-owns={open ? 'mouse-over-popover' : undefined} />
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Avatar>W</Avatar>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Progress percent={70} status="exception" />
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Avatar>W</Avatar>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Progress percent={100} />
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Avatar>W</Avatar>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Progress percent={50} showInfo={false} />
                                </Grid>
                            </Grid>
                        </Paper>

                        {/* </Paper> */}
                    </Card>
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item xs={6}>
                    <Card>
                        <CardHeader color="success" >
                            <Grid container spacing={1}>
                                <Grid item xs={6}>
                                    <a>Proyek A</a>
                                </Grid>
                                <Grid item xs={2}>
                                </Grid>
                                <Grid item xs={2}>
                                    <Progress type="circle" width={60} percent={75} format={percent => `${percent} Days`} />
                                </Grid>
                                <Grid item xs={2}>
                                    <Tooltip title="Add" aria-label="add" >
                                        <Fab color="primary" className={classes.fab}>
                                            <AddIcon />
                                        </Fab>
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </CardHeader>
                        {/* <Paper className={classes.paper}> */}
                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Tooltip title='aslam'>
                                        <Avatar src="http://127.0.0.1/gambar/2.jpg" aria-owns={open ? 'mouse' : undefined}
                                        />
                                    </Tooltip>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Tooltip title='progress pekerjaan 1' >
                                        <Progress percent={30} />
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                <Tooltip title='lena'>
                                    <Avatar alt="marc" src="http://127.0.0.1/gambar/111.jpg" aria-owns={open ? 'mouse' : undefined}/>
                                    </Tooltip>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Tooltip title='progress pekerjaan 2' >
                                        <Progress percent={50} status="active" aria-owns={open ? 'mouse-over-popover' : undefined} />
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Avatar>W</Avatar>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Progress percent={70} status="exception" />
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Avatar>W</Avatar>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Progress percent={100} />
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Avatar>W</Avatar>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Progress percent={50} showInfo={false} />
                                </Grid>
                            </Grid>
                        </Paper>

                        {/* </Paper> */}
                    </Card>
                </Grid>
                <Grid item xs={6}>
                    <Card>
                        <CardHeader color="success" >
                            <Grid container spacing={1}>
                                <Grid item xs={6}>
                                    <a>Proyek A</a>
                                </Grid>
                                <Grid item xs={2}>
                                </Grid>
                                <Grid item xs={2}>
                                    <Progress type="circle" width={60} percent={75} format={percent => `${percent} Days`} />
                                </Grid>
                                <Grid item xs={2}>
                                    <Tooltip title="Add" aria-label="add" >
                                        <Fab color="primary" className={classes.fab}>
                                            <AddIcon />
                                        </Fab>
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </CardHeader>
                        {/* <Paper className={classes.paper}> */}
                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Tooltip title='aslam'>
                                        <Avatar src="http://127.0.0.1/gambar/2.jpg" aria-owns={open ? 'mouse' : undefined}
                                        />
                                    </Tooltip>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Tooltip title='progress pekerjaan 1' >
                                        <Progress percent={30} />
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                <Tooltip title='lena'>
                                    <Avatar alt="marc" src="http://127.0.0.1/gambar/111.jpg" aria-owns={open ? 'mouse' : undefined}/>
                                    </Tooltip>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Tooltip title='progress pekerjaan 2' >
                                        <Progress percent={50} status="active" aria-owns={open ? 'mouse-over-popover' : undefined} />
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Avatar>W</Avatar>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Progress percent={70} status="exception" />
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Avatar>W</Avatar>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Progress percent={100} />
                                </Grid>
                            </Grid>
                        </Paper>

                        <Paper className={classes.paper}>
                            <Grid container wrap="nowrap" spacing={1}>
                                <Grid item>
                                    <Avatar>W</Avatar>
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Progress percent={50} showInfo={false} />
                                </Grid>
                            </Grid>
                        </Paper>

                        {/* </Paper> */}
                    </Card>
                </Grid>
            </Grid>
        </div>
        
    );
}