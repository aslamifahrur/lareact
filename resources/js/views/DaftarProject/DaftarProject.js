import React, { useState, useEffect } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import Table from "../../components/Table/Table.js";
import Card from "../../components/Card/Card.js";
import CardHeader from "../../components/Card/CardHeader.js";
import CardBody from "../../components/Card/CardBody.js";
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import TreeView from '@material-ui/lab/TreeView';
import TreeItem from '@material-ui/lab/TreeItem';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  },
  active: {
    backgroundColor: "#cfdfff"
  },
  item_header: {
    fontWeight: 'light',
    paddingLeft: '0px',
    textAlign: 'left'
  },
  root: {
    height: 216,
    flexGrow: 1,
    // maxWidth: 400,
  },
};
const useStyles = makeStyles(styles);

const useTreeItemStyles = makeStyles(theme => ({
  root: {
    color: theme.palette.text.secondary,
    '&:hover > $content': {
      backgroundColor: theme.palette.action.hover,
    },
    '&:focus > $content, &$selected > $content': {
      backgroundColor: `var(--tree-view-bg-color, ${theme.palette.grey[400]})`,
      color: 'var(--tree-view-color)',
    },
    '&:focus > $content $label, &:hover > $content $label, &$selected > $content $label': {
      backgroundColor: 'transparent',
    },
  },
  content: {
    color: theme.palette.text.secondary,
    borderTopRightRadius: theme.spacing(2),
    borderBottomRightRadius: theme.spacing(2),
    paddingRight: theme.spacing(1),
    fontWeight: theme.typography.fontWeightMedium,
    '$expanded > &': {
      fontWeight: theme.typography.fontWeightRegular,
    },
  },
  group: {
    marginLeft: 0,
    '& $content': {
      paddingLeft: theme.spacing(2),
    },
  },
  expanded: {},
  selected: {},
  label: {
    fontWeight: 'inherit',
    color: 'inherit',
  },
  labelRoot: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0.5, 0),
  },
  labelIcon: {
    marginRight: theme.spacing(1),
  },
  labelText: {
    fontWeight: 'inherit',
    flexGrow: 1,
  },
}));

function DaftarProject() {
  const [daftar_project, setDaftarProject] = useState([]);
  const [selectedProject, setSelectedProject] = useState({});
  const [daftar_kelompok_tugas, setDaftarKelompokTugas] = useState([]);
  // const [daftar_tugas,setDaftarTugas] = useState({});
  const [expanded, setExpanded] = React.useState([]);
  const [selected, setSelected] = React.useState([]);

  const handleToggle = (event, nodeIds) => {
    setExpanded(nodeIds);
  };

  const handleSelect = (event, nodeIds) => {
    setSelected(nodeIds);
  };

  const classes = useStyles();

  useEffect(() => {
    fetch("http://localhost:8000/api/v1/get_projects")
      .then(res => res.json())
      .then(data => {
        // console.log(data);
        setDaftarProject(data);
        if (data.length > 0) setSelectedProject(data[0]);
      }).catch(error => console.log(error))
  }, [])

  useEffect(() => {
    fetch("http://localhost:8000/api/v1/get_kelompok_tugas/" + selectedProject.id)
      .then(res => res.json())
      .then(data => {
        // console.log(data);
        setDaftarKelompokTugas(data);
      }).catch(error => console.log(error))
  }, [selectedProject])

  return (
    <GridContainer>
      <GridItem xs={6} sm={6} md={6}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>Daftar Project</h4>
          </CardHeader>
          <CardBody>
            <ButtonGroup
              orientation="vertical"
              color="default"
              aria-label="vertical primary button group"
              fullWidth
              variant='text'
            >
              {daftar_project.map((item) => {
                return <Button className={item.id == selectedProject.id ? classes.active : ''} onClick={() => setSelectedProject(item)} key={item.id}>{item.id + " - " + item.nama}</Button>;
              })}
            </ButtonGroup>
          </CardBody>
        </Card>
      </GridItem>
      {selectedProject.id != null ?
        <GridItem xs={6} sm={6} md={6}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Breakdown Project</h4>
              <p>Project : {selectedProject.id + " - " + selectedProject.nama}</p>
            </CardHeader>
            <CardBody>
              <TreeView
                className={classes.root}
                defaultCollapseIcon={<ExpandMoreIcon />}
                defaultExpandIcon={<ChevronRightIcon />}
                expanded={expanded}
                selected={selected}
                onNodeToggle={handleToggle}
                onNodeSelect={handleSelect}
              >
                {
                  daftar_kelompok_tugas.map(item => {
                    return (
                      <TreeItem key={item.id_proyek + item.id} nodeId={item.id_proyek+item.id} label={<div>{item.kelompok}<span style={{ float: 'right' }}>Tes</span></div>}>
                        {
                          daftar_kelompok_tugas.map(anu=>{
                            return(
                              <TreeItem key={item.id} nodeId={item.id + ''} label={<div>{item.kelompok}<span style={{ float: 'right' }}>Tes</span></div>}/>
                            )
                          })
                        }
                      </TreeItem>
                    );
                  })
                }
                {/* <TreeItem nodeId="1" label="Applications">
                  <TreeItem nodeId="2" label="Calendar" />
                  <TreeItem nodeId="3" label="Chrome" />
                  <TreeItem nodeId="4" label="Webstorm" />
                </TreeItem>
                <TreeItem nodeId="5" label="Documents">
                  <TreeItem nodeId="6" label="Material-UI">
                    <TreeItem nodeId="7" label="src">
                      <TreeItem nodeId="8" label="index.js" />
                      <TreeItem nodeId="9" label="tree-view.js" />
                    </TreeItem>
                  </TreeItem>
                </TreeItem> */}
              </TreeView>
            </CardBody>
          </Card>
        </GridItem>
        : null
      }
    </GridContainer>
  );
}

export default DaftarProject;
