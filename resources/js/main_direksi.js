import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

// core components
import Admin from "./layouts/Admin_direksi.js";
// import RTL from "./layouts/RTL.js";

import "./assets/css/material-dashboard-react.css?v=1.8.0";

const hist = createBrowserHistory();

ReactDOM.render(
  <Router history={hist}>
    <Switch>
      <Route path="/direksi" component={Admin} />
      {/* <Route path="/kadiv/rtl" component={RTL} /> */}
      <Redirect from="/direksi" to="/direksi/dashboard" />
    </Switch>
  </Router>,
  document.getElementById("root")
);