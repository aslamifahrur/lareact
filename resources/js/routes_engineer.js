/*!

=========================================================
* Material Dashboard React - v1.8.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
import LocationOn from "@material-ui/icons/LocationOn";
import Notifications from "@material-ui/icons/Notifications";
import Unarchive from "@material-ui/icons/Unarchive";
import Language from "@material-ui/icons/Language";
import BorderColor from "@material-ui/icons";
// core components/views for Admin layout
import DashboardPage from "./views/Dashboard/Dashboard.js";
import UserProfile from "./views/UserProfile/UserProfile.js";
import TableList from "./views/TableList/TableList.js";
import Typography from "./views/Typography/Typography.js";
import Icons from "./views/Icons/Icons.js";
import Maps from "./views/Maps/Maps.js";
import NotificationsPage from "./views/Notifications/Notifications.js";
import UpgradeToPro from "./views/UpgradeToPro/UpgradeToPro.js";
// core components/views for RTL layout
import RTLPage from "./views/RTLPage/RTLPage.js";
//komponen tambahan
import tambah from "./views/tambah/tambah.js";
import Paper from "./views/Paper/Paper.js";
import LkhEngineer from"./views/LkhEngineer/Engine.js";
import Kadiv from "./views/LkhKadiv/Kadiv.js";
import LaporanLkh from"./views/LaporanLkhEng/LaporanLkh.js";  

const dashboardRoutes = [
  // {
  //   path: "/dashboard",
  //   name: "Dashboard",
  //   rtlName: "لوحة القيادة",
  //   icon: Dashboard,
  //   component: DashboardPage,
  //   layout: "/engineer"
  // },
  // {
  //   path: "/user",
  //   name: "User Profile",
  //   rtlName: "ملف تعريفي للمستخدم",
  //   icon: Person,
  //   component: UserProfile,
  //   layout: "/engineer"
  // },
  // {
  //   path: "/table",
  //   name: "Table List",
  //   rtlName: "قائمة الجدول",
  //   icon: "content_paste",
  //   component: TableList,
  //   layout: "/engineer"
  // },
  // {
  //   path: "/typography",
  //   name: "Typography",
  //   rtlName: "طباعة",
  //   icon: LibraryBooks,
  //   component: Typography,
  //   layout: "/engineer"
  // },
  // {
  //   path: "/icons",
  //   name: "Icons",
  //   rtlName: "الرموز",
  //   icon: BubbleChart,
  //   component: Icons,
  //   layout: "/engineer"
  // },
  // {
  //   path: "/maps",
  //   name: "Maps",
  //   rtlName: "خرائط",
  //   icon: LocationOn,
  //   component: Maps,
  //   layout: "/engineer"
  // },
  // {
  //   path: "/notifications",
  //   name: "Notifications",
  //   rtlName: "إخطارات",
  //   icon: Notifications,
  //   component: NotificationsPage,
  //   layout: "/engineer"
  // },
  // {
  //   path: "/Kadiv",
  //   name: "Kadiv",
  //   rtlName: "پشتیبانی از راست به چپ",
  //   icon: Language,
  //   component: Kadiv,
  //   layout: "/engineer"
  // },
    {
    path: "/LkhEngineer",
    name: "LkhEngineer",
    rtlName: "إخطارات",
    icon: LocationOn,
    component: LkhEngineer,
    layout: "/engineer"
  },
  {
    path: "/LaporanLkh",
    name: "LaporanLkh",
    rtlName: "إخطارات",
    icon: LocationOn,
    component: LaporanLkh,
    layout: "/engineer"
  },
  {
    path: "/paper",
    name: "paper",
    rtlName: "إخطارات",
    icon: LocationOn,
    component: Paper,
    layout: "/engineer"
  }
  // {
  //   path: "/upgrade-to-pro",
  //   name: "Upgrade To PRO",
  //   rtlName: "التطور للاحترافية",
  //   icon: Unarchive,
  //   component: UpgradeToPro,
  //   layout: "/engineer"
  // }
];

export default dashboardRoutes;
