<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Terafulk | Laravel</title>

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">

    <style>
        html, body{
            height:100%;
            width: 100%;
            margin: 0;
            padding:0;
            display: flex;
            background-color: dimgrey;
        }
        #app{
            width: 100%;
            display: grid;
            place-items: center;
            padding:0;
            margin: 0;
        }
        .form_login{
            width: 90%;
            max-width:450px;
        }
    </style>
</head>
<body>
    <div id="app">
        <div class="panel panel-primary form_login">
            <div class="panel-header" style="display:grid;place-items:center;background-color:#d0e0f0;margin:0;padding:0;">
                <img src="{{asset('images/logo.png')}}" style="height:80%" alt="">
            </div>

            <div class="panel-body" style="padding-top:30px;background-color: #cfcfcf;">
                <form method="POST" action="{{ route('loginadmin') }}" class="form-horizontal">
                    @csrf

                    <div class="form-group row">
                        <label for="email" class="col-md-4 control-label">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-8">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 control-label">{{ __('Password') }}</label>

                        <div class="col-md-8">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <br>
                    <div class="form-group row">
                        <div class="col-md-8 post">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Login') }}
                            </button>

                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>



