<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DireksiController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth:direksi');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('direksi');
    }
}