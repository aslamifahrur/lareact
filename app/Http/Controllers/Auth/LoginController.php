<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function postloginadmin(Request $request){
        $email=$request->email;
        $password=$request->password;
        $remember=$request->remember;


        if(Auth::guard('direksi')->attempt(['email'=>$email,'password'=>$password,'level'=>'Direksi'],$remember)){
            Auth::attempt(['email'=>$email,'password'=>$password]);
            return redirect()->to(route('direksi'));
        }
        if(Auth::guard('kadiv')->attempt(['email'=>$email,'password'=>$password,'level'=>'Kadiv'],$remember)){
            Auth::attempt(['email'=>$email,'password'=>$password]);
            return redirect()->to(route('kadiv'));
        }
        if(Auth::guard('engineer')->attempt(['email'=>$email,'password'=>$password,'level'=>'Engineer'],$remember)){
            Auth::attempt(['email'=>$email,'password'=>$password]);
            return redirect()->to(route('engineer'));
        }
        return redirect()->to(route('login'));
    }
}
